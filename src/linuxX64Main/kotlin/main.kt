package org.example.basicGui

import gtk3.GtkWindowPosition
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application

internal lateinit var mainWin: MainWindow

fun main() {
    guiApplicationActivateHandler = ::activateApplication
    // Creation and setup of the application takes place in this function.
    GuiApplication.create(id = "org.example.Basicgui").use {
        mainWin = MainWindow(this)
        assignDefaultActivateHandler()
        // Run the application, and print out the application status.
        println("Application Status: ${run()}")
    }
}

private fun activateApplication(app: Application) {
    println("App ID: ${app.appId}")
    // The default application UI is created in this function.
    mainWin.createUi {
        title = "Basic GUI"
        changePosition(GtkWindowPosition.GTK_WIN_POS_CENTER)
        visible = true
    }
}
