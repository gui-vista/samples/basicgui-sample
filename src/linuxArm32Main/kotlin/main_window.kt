package org.example.basicGui

import glib2.gpointer
import gtk3.GtkButton
import gtk3.GtkOrientation
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.boxLayout
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.gui.widget.button.buttonWidget
import io.gitlab.guiVista.gui.widget.dataEntry.entryWidget
import io.gitlab.guiVista.gui.widget.display.labelWidget
import io.gitlab.guiVista.gui.widget.widgetActivateHandler
import io.gitlab.guiVista.gui.window.AppWindow
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction

internal class MainWindow(app: GuiApplication) : AppWindow(app) {
    // All individual widgets have to be created lazily otherwise a segmentation fault WILL occur. Is is good practise
    // to make all widgets used in a window private.
    private val nameEntry by lazy { createNameEntry() }
    private val greetingLbl by lazy { createGreetingLbl() }

    init {
        widgetActivateHandler = ::handleDefaultAction
    }

    private fun createGreetingLbl() = labelWidget(text = "")

    override fun createMainLayout(): ContainerBase = boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_VERTICAL) {
        spacing = 20
        changeAllMargins(5)
        appendChild(greetingLbl)
        appendChild(createInputLayout())
    }

    private fun createInputLayout() = boxLayout {
        spacing = 5
        appendChild(nameEntry)
        appendChild(createGreetingBtn())
    }

    private fun createNameEntry() = entryWidget {
        text = ""
        placeholderText = "Enter name"
        Widget.fromWidgetBase(this).assignDefaultActivateHandler()
    }

    private fun createGreetingBtn() = buttonWidget(label = "Display Greeting") {
        connectClickedEvent(staticCFunction(::greetingBtnClicked))
    }

    // Below is an example of controlling/limiting access to some widgets (aka the "Gatekeeper" technique). Note that
    // there is NO direct access to the widgets. This is considered good practise.
    fun updateGreeting() {
        greetingLbl.text = "Hello ${nameEntry.text}! :)"
        nameEntry.text = ""
        resetFocus()
    }

    override fun resetFocus() {
        // TODO: Workaround Entry.placeholderText bug that occurs when the Entry (nameEntry) has focus.
        nameEntry.grabFocus()
    }
}

@Suppress("UNUSED_PARAMETER")
private fun handleDefaultAction(widget: WidgetBase) {
    mainWin.updateGreeting()
}

@Suppress("UNUSED_PARAMETER")
private fun greetingBtnClicked(btn: CPointer<GtkButton>, userData: gpointer) {
    mainWin.updateGreeting()
}
