group = "org.guivista"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.5.30"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val guiVistaVer = "0.5.0"
    val guiVistaGroupId = "io.gitlab.gui-vista"
    linuxX64 {
        binaries {
            executable("basic_gui") {
                entryPoint = "org.example.basicGui.main"
            }
        }
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaVer")
            }
        }
    }

    linuxArm32Hfp("linuxArm32") {
        binaries {
            executable("basic_gui") {
                entryPoint = "org.example.basicGui.main"
            }
        }
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaVer")
            }
        }
    }
}
